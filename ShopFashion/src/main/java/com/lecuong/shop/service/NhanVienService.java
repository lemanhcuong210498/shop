package com.lecuong.shop.service;

import com.lecuong.shop.entity.NhanVien;

public interface NhanVienService{

    boolean kiemTraDangNhap(String email, String matkhau);

    boolean themNhanVien(NhanVien nhanVien);
}
