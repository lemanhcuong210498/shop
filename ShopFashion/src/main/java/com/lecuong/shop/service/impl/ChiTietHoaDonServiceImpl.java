package com.lecuong.shop.service.impl;

import com.lecuong.shop.dao.ChiTietHoaDonDAO;
import com.lecuong.shop.entity.ChiTietHoaDon;
import com.lecuong.shop.service.ChiTietHoaDonServeice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChiTietHoaDonServiceImpl implements ChiTietHoaDonServeice {

    @Autowired
    private ChiTietHoaDonDAO chiTietHoaDonDAO;

    @Override
    public boolean themChiTietHoaDon(ChiTietHoaDon chiTietHoaDon) {

        return chiTietHoaDonDAO.themChiTietHoaDon(chiTietHoaDon);
    }
}
