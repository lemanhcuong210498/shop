package com.lecuong.shop.service.impl;

import com.lecuong.shop.dao.DanhMucDAO;
import com.lecuong.shop.entity.DanhMucSanPham;
import com.lecuong.shop.service.DanhMucService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DanhMucServiceImpl implements DanhMucService {

    @Autowired
    private DanhMucDAO danhMucDAO;

    @Override
    public List<DanhMucSanPham> layDanhMuc() {
        return danhMucDAO.layDanhMucSanPham();
    }
}
