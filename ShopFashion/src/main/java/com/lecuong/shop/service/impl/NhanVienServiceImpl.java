package com.lecuong.shop.service.impl;

import com.lecuong.shop.dao.NhanVienDAO;
import com.lecuong.shop.entity.NhanVien;
import com.lecuong.shop.service.NhanVienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NhanVienServiceImpl implements NhanVienService {

    @Autowired
    private NhanVienDAO nhanVienDAO;

    @Override
    public boolean kiemTraDangNhap(String email, String matkhau) {

        boolean kiemTra = nhanVienDAO.kiemTraDangNhap(email,matkhau);

        return kiemTra;
    }

    @Override
    public boolean themNhanVien(NhanVien nhanVien) {

        boolean kiemTra = nhanVienDAO.themNhanVien(nhanVien);

        return kiemTra;
    }
}
