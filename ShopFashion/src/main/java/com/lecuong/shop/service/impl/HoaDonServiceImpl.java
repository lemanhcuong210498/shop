package com.lecuong.shop.service.impl;

import com.lecuong.shop.dao.HoaDonDAO;
import com.lecuong.shop.entity.HoaDon;
import com.lecuong.shop.service.HoaDonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HoaDonServiceImpl implements HoaDonService {

    @Autowired
    private HoaDonDAO hoaDonDAO;

    @Override
    public int themHoaDon(HoaDon hoaDon) {

        return hoaDonDAO.themHoaDon(hoaDon);
    }
}
