package com.lecuong.shop.service;

import com.lecuong.shop.entity.DanhMucSanPham;

import java.util.List;

public interface DanhMucService {

    List<DanhMucSanPham> layDanhMuc();
}
