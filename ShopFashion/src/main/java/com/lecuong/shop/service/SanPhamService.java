package com.lecuong.shop.service;

import com.lecuong.shop.entity.ChiTietSanPham;
import com.lecuong.shop.entity.SanPham;

import java.util.List;

public interface SanPhamService {

    List<SanPham> layDanhSachSanPham(int sanPhamBatDau);

    SanPham layDanhSachChiTietSanPhamTheoID(int idSanPham);

    List<SanPham> laySanPhamTheoDanhMuc(int idDanhMucSanPham);
}
