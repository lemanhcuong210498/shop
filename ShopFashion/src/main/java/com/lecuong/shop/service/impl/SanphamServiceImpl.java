package com.lecuong.shop.service.impl;

import com.lecuong.shop.dao.SanPhamDAO;
import com.lecuong.shop.entity.SanPham;
import com.lecuong.shop.service.SanPhamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SanphamServiceImpl implements SanPhamService {

    @Autowired
    private SanPhamDAO sanPhamDAO;

    @Override
    public List<SanPham> layDanhSachSanPham(int sanPhamBatDau) {

        return sanPhamDAO.layDanhSachSanPham(sanPhamBatDau);
    }

    @Override
    public SanPham layDanhSachChiTietSanPhamTheoID(int idSanPham) {

        return sanPhamDAO.layDanhSachChiTietSanPhamTheoID(idSanPham);
    }

    @Override
    public List<SanPham> laySanPhamTheoDanhMuc(int idDanhMucSanPham) {
        return sanPhamDAO.laySanPhamTheoDanhMuc(idDanhMucSanPham);
    }
}
