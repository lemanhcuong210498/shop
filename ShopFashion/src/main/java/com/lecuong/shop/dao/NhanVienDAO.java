package com.lecuong.shop.dao;

import com.lecuong.shop.entity.NhanVien;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NhanVienDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public boolean kiemTraDangNhap(String email, String matkhau) {

        Session session = sessionFactory.openSession();

        try {
            NhanVien nhanVien = (NhanVien) session.
                    createQuery("from NhanVien where email = '"+email+"' and matKhau = '"+matkhau+"'").getSingleResult();
            if (nhanVien != null) {
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            return false;
        }finally {
            session.close();
        }
    }

    public boolean themNhanVien(NhanVien nhanVien){

        Session session = sessionFactory.openSession();

        int idNhanVien = (int) session.save(nhanVien);

        if (idNhanVien > 0){
            return true;
        }else{
            return false;
        }
    }

}
