package com.lecuong.shop.dao;

import com.lecuong.shop.entity.DanhMucSanPham;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class DanhMucDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public List<DanhMucSanPham> layDanhMucSanPham(){

        Session session = sessionFactory.getCurrentSession();

        String query = "from DanhMucSanPham";

        List<DanhMucSanPham> listDanhMucSanPham = session.createQuery(query).getResultList();

        return listDanhMucSanPham;
    }
}
