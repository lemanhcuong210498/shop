package com.lecuong.shop.dao;

import com.lecuong.shop.entity.ChiTietSanPham;
import com.lecuong.shop.entity.SanPham;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class SanPhamDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public List<SanPham> layDanhSachSanPham(int sanPhamBatDau){

        Session session = sessionFactory.getCurrentSession();

        List<SanPham> listSanPham =
                (List<SanPham>) session.createQuery("from SanPham").setFirstResult(sanPhamBatDau).setMaxResults(10).getResultList();

        return listSanPham;
    }

    @Transactional
    public SanPham layDanhSachChiTietSanPhamTheoID(int idSanPham){

        Session session = sessionFactory.getCurrentSession();

        String query = "from SanPham where idSanPham = "+idSanPham;

        SanPham sanPham = (SanPham) session.createQuery(query).getSingleResult();

        return sanPham;
    }

    @Transactional
    public List<SanPham> laySanPhamTheoDanhMuc(int idDanhMucSanPham){

        Session session = sessionFactory.getCurrentSession();

        String query = "from SanPham where danhMucSanPham.idDanhMucSanPham = "+idDanhMucSanPham;

        List<SanPham> listSanPham = (List<SanPham>) session.createQuery(query).getResultList();

        return listSanPham;
    }
}
