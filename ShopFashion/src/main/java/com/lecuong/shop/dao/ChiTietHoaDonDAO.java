package com.lecuong.shop.dao;

import com.lecuong.shop.entity.ChiTietHoaDon;
import com.lecuong.shop.entity.ChiTietHoaDonId;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ChiTietHoaDonDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public boolean themChiTietHoaDon(ChiTietHoaDon chiTietHoaDon){

        Session session = sessionFactory.getCurrentSession();

        ChiTietHoaDonId id = (ChiTietHoaDonId) session.save(chiTietHoaDon);

        if (id != null){
            return true;
        }else {
            return false;
        }
    }
}
