package com.lecuong.shop.controller;

import com.lecuong.shop.entity.ChucVu;
import com.lecuong.shop.entity.NhanVien;
import com.lecuong.shop.service.NhanVienService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
@RequestMapping("/dangnhap")
public class LoginController {

    @Autowired
    private NhanVienService nhanVienService;

    @GetMapping
    public String loginPage(){
        return "dangnhap";
    }

    @PostMapping
    public String dangKy(@RequestParam String email,
                         @RequestParam String matkhau,
                         @RequestParam String nhaplaimatkhau,
                         ModelMap modelMap){

        boolean checkEmail = validate(email);

        if (checkEmail){
            if (matkhau.equals(nhaplaimatkhau)){
                NhanVien nhanVien = new NhanVien();
                nhanVien.setEmail(email);
                nhanVien.setMatKhau(matkhau);
                nhanVien.setTenDangNhap(email);

                boolean ktThem = nhanVienService.themNhanVien(nhanVien);

                if (ktThem){
                    modelMap.addAttribute("kiemtradangnhap","Tạo tài khoản thành công");
                }else{
                    modelMap.addAttribute("kiemtradangnhap","Tạo tài khoản thất bại");
                }
            }else{
                modelMap.addAttribute("kiemtradangnhap", "Mật khẩu không trùng khớp");
            }
        }else{
            modelMap.addAttribute("kiemtradangnhap","Vui lòng nhập đúng định dạng email");
        }

        return "dangnhap";
    }

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9._]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean validate(String email){
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }
}
