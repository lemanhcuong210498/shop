package com.lecuong.shop.controller;

import com.lecuong.shop.entity.ChiTietHoaDon;
import com.lecuong.shop.entity.ChiTietHoaDonId;
import com.lecuong.shop.entity.GioHang;
import com.lecuong.shop.entity.HoaDon;
import com.lecuong.shop.service.ChiTietHoaDonServeice;
import com.lecuong.shop.service.HoaDonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/giohang")
public class GioHangController {

    @Autowired
    private HoaDonService hoaDonService;

    @Autowired
    private ChiTietHoaDonServeice chiTietHoaDonServeice;

    @GetMapping
    public String gioHangPage(ModelMap modelMap, HttpSession httpSession){

        //Kiem tra xem session user co null hay ko va lay ra chu cai dau trong username
        if (httpSession.getAttribute("user") != null){

            String tenDangNhap = (String) httpSession.getAttribute("user");

            String chuCaiDau = tenDangNhap.substring(0,1);

            modelMap.addAttribute("chucaidau",chuCaiDau);
        }

        //Lay ra so luong san pham trong gio hang
        if(null != httpSession.getAttribute("giohang")){
            List<GioHang> listGioHang = (List<GioHang>) httpSession.getAttribute("giohang");
            modelMap.addAttribute("soluongsanphamgiohang", listGioHang.size());
            modelMap.addAttribute("listGioHang", listGioHang);
        }
        return "giohang";
    }

    @PostMapping
    public String themHoaDon(@RequestParam String tenKhachHang,
                             @RequestParam String soDienThoai,
                             @RequestParam String diaChiGiaoHang,
                             @RequestParam String hinhThucGiaoHang,
                             @RequestParam String ghiChu,
                             HttpSession httpSession){

        if(null != httpSession.getAttribute("giohang")){
            List<GioHang> listGioHang = (List<GioHang>) httpSession.getAttribute("giohang");
            HoaDon hoaDon = new HoaDon();
            hoaDon.setTenKhachHang(tenKhachHang);
            hoaDon.setSoDienThoai(soDienThoai);
            hoaDon.setDiaChiGiaoHang(diaChiGiaoHang);
            hoaDon.setHinhThucGiaoHang(hinhThucGiaoHang);
            hoaDon.setGhiChu(ghiChu);

            int idHoaDon = hoaDonService.themHoaDon(hoaDon);

            if (idHoaDon > 0){
                Set<ChiTietHoaDon> listChiTietHoaDon = new HashSet<>();

                for(GioHang gioHang : listGioHang){
                    ChiTietHoaDonId chiTietHoaDonId = new ChiTietHoaDonId();
                    chiTietHoaDonId.setIdchiTietSanPham(gioHang.getIdChiTietSanPham());
                    chiTietHoaDonId.setIdHoaDon(hoaDon.getIdHoaDon());

                    ChiTietHoaDon chiTietHoaDon = new ChiTietHoaDon();
                    chiTietHoaDon.setChiTietHoaDonId(chiTietHoaDonId);
                    chiTietHoaDon.setGiaTien(gioHang.getGiaTien());
                    chiTietHoaDon.setSoLuong(gioHang.getSoLuong());

                    chiTietHoaDonServeice.themChiTietHoaDon(chiTietHoaDon);
                }
            }else{

            }


        }

        return "giohang";
    }
}
