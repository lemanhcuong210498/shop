package com.lecuong.shop.controller;

import com.lecuong.shop.entity.ChiTietSanPham;
import com.lecuong.shop.entity.DanhMucSanPham;
import com.lecuong.shop.entity.GioHang;
import com.lecuong.shop.entity.SanPham;
import com.lecuong.shop.service.DanhMucService;
import com.lecuong.shop.service.SanPhamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/chitiet")
public class ChiTietController {

    @Autowired
    private SanPhamService sanPhamService;

    @Autowired
    private DanhMucService danhMucService;

    @GetMapping
    public String chiTietPage(@RequestParam int idsanpham,
                              ModelMap modelMap,
                              HttpSession httpSession){

        //Kiem tra xem session user co null hay ko va lay ra chu cai dau trong username
        if (httpSession.getAttribute("user") != null){

            String tenDangNhap = (String) httpSession.getAttribute("user");

            String chuCaiDau = tenDangNhap.substring(0,1);

            modelMap.addAttribute("chucaidau",chuCaiDau);
        }

        SanPham sanPham = sanPhamService.layDanhSachChiTietSanPhamTheoID(idsanpham);
        modelMap.addAttribute("sanpham", sanPham);

        //menu cac danh muc san pham
        List<DanhMucSanPham> listDanhMucSanPham = danhMucService.layDanhMuc();
        modelMap.addAttribute("danhmuc", listDanhMucSanPham);

        //Lay ra so luong san pham trong gio hang
        if(null != httpSession.getAttribute("giohang")){
            List<GioHang> listGioHang = (List<GioHang>) httpSession.getAttribute("giohang");
            modelMap.addAttribute("soluongsanphamgiohang", listGioHang.size());
        }

        return "chitiet";
    }
}
