package com.lecuong.shop.controller;

import com.lecuong.shop.entity.DanhMucSanPham;
import com.lecuong.shop.entity.GioHang;
import com.lecuong.shop.entity.SanPham;
import com.lecuong.shop.service.DanhMucService;
import com.lecuong.shop.service.SanPhamService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    private SanPhamService sanPhamService;

    @Autowired
    private DanhMucService danhMucService;

    @GetMapping
    public String homePage(ModelMap modelMap, HttpSession httpSession){

        //Kiem tra xem session user co null hay ko va lay ra chu cai dau trong username
        if (httpSession.getAttribute("user") != null){

            String tenDangNhap = (String) httpSession.getAttribute("user");

            String chuCaiDau = tenDangNhap.substring(0,1);

            modelMap.addAttribute("chucaidau",chuCaiDau);
        }

        //lay ra session gio hang
        if(null != httpSession.getAttribute("giohang")){
            List<GioHang> listGioHang = (List<GioHang>) httpSession.getAttribute("giohang");
            modelMap.addAttribute("soluongsanphamgiohang", listGioHang.size());
        }

        //Load danh sach san pham
        List<SanPham> listSanPham = sanPhamService.layDanhSachSanPham(0);
        modelMap.addAttribute("listSanPham", listSanPham);

        //menu cac danh muc san pham
        List<DanhMucSanPham> listDanhMucSanPham = danhMucService.layDanhMuc();
        modelMap.addAttribute("danhmuc", listDanhMucSanPham);

        return "trangchu";
    }
}
