package com.lecuong.shop.controller;

import com.lecuong.shop.entity.DanhMucSanPham;
import com.lecuong.shop.entity.SanPham;
import com.lecuong.shop.service.DanhMucService;
import com.lecuong.shop.service.SanPhamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/sanpham")
public class SanPhamController {

    @Autowired
    private DanhMucService danhMucService;

    @Autowired
    private SanPhamService sanPhamService;

    @GetMapping
    public String sanPhamPage(ModelMap modelMap,
                              @RequestParam int idDanhMucSanPham,
                              @RequestParam String tenDanhMuc,
                              HttpSession httpSession){

        //Kiem tra xem session user co null hay ko va lay ra chu cai dau trong username
        if (httpSession.getAttribute("user") != null){

            String tenDangNhap = (String) httpSession.getAttribute("user");

            String chuCaiDau = tenDangNhap.substring(0,1);

            modelMap.addAttribute("chucaidau",chuCaiDau);
        }

        //menu cac danh muc san pham
        List<DanhMucSanPham> listDanhMucSanPham = danhMucService.layDanhMuc();
        modelMap.addAttribute("danhmuc", listDanhMucSanPham);

        List<SanPham> listSanPham = sanPhamService.laySanPhamTheoDanhMuc(idDanhMucSanPham);
        modelMap.addAttribute("listSanPham", listSanPham);
        modelMap.addAttribute("tenDanhMuc", tenDanhMuc);

        return "sanpham";
    }
}
