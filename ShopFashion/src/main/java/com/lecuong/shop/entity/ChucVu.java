package com.lecuong.shop.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "chucvu")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChucVu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idchucvu")
    private int idChucVu;

    @Column(name = "tenchucvu")
    private String tenChucVu;
}
