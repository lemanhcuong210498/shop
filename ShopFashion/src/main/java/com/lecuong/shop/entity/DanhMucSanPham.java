package com.lecuong.shop.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "danhmucsanpham")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DanhMucSanPham {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iddanhmucsanpham")
    private int idDanhMucSanPham;

    @Column(name = "tendanhmuc")
    private String tenDanhMuc;

//    @Column(name = "hinhdanhmuc")
//    private String hinhDanhMuc;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "iddanhmucsanpham")
    private Set<SanPham> danhSachSanPham;
}
