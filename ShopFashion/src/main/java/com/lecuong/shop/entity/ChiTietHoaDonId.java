package com.lecuong.shop.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@Embeddable
public class ChiTietHoaDonId implements Serializable {

    int idHoaDon;
    int idchiTietSanPham;
}
