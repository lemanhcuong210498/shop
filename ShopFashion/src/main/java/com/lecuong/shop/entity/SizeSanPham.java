package com.lecuong.shop.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "sizesanpham")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SizeSanPham {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idsizesanpham")
    private int idSizeSanPham;

    @Column(name = "size")
    private String size;
}
