package com.lecuong.shop.entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "khuyenmai")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KhuyenMai {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idkhuyenmai")
    private int idKhuyenMai;

    @Column(name = "tenkhuyenmai")
    private String tenKhuyenMai;

    @Column(name = "thoigianbatdau")
    private String thoiGianBatDau;

    @Column(name = "thoigianketthuc")
    private String thoiGianKetThuc;

    @Column(name = "mota")
    private String moTa;

    @Column(name = "hinhkhuyenmai")
    private String hinhKhuyenMai;

    @Column(name = "giagiam")
    private int giaGiam;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "chitietkhuyenmai",
                    joinColumns = {@JoinColumn(name = "idkhuyenmai", referencedColumnName = "idkhuyenmai")},
                    inverseJoinColumns = {@JoinColumn(name = "idsanpham", referencedColumnName = "idsanpham")})
    private Set<SanPham> danhSachSanPham = new HashSet<>();
}
