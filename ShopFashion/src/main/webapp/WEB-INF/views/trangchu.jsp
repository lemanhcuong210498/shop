<!--Author: Le Manh Cuong, Date: 11/08/2020 10:38AM -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href='<c:url value="css/styles.css" />' />
    <!-- <link rel="stylesheet" href="./css/bootstrap.min.css" /> -->
    <script src='<c:url value="js/jquery-3.5.1.min.js" />'></script>
    <link rel="stylesheet" href='<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />' />
    <script src='<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" />'></script>
    <script src='<c:url value="https://code.jquery.com/jquery-3.2.1.slim.min.js" />'></script>
    <script src='<c:url value="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" />'></script>
    <link href='<c:url value="fonts/Roboto.css" />' rel='stylesheet' />
    <link rel="stylesheet" href='<c:url value="css/animate.css" />' />
    <script src='<c:url value="js/wow.min.js" />'></script>
    <script>new WOW().init();</script>
    <style>
        .sanpham.wow.bounceIn {
            height: 460px;
        }

        .sanpham.wow.bounceIn span {
            height: 93px;
            overflow: hidden;
            display: block;
            font-size: 20px;
            width: 90%;
            margin: auto;
        }
    </style>
    <title>Trang chủ</title>
</head>
<body>
    <!--Header -->
    <jsp:include page="/WEB-INF/views/header.jsp"></jsp:include>
    <!-- End Header -->

    <!--Body-->
    <div id="info" class="container">
        <div class="row">
            <div class="col-12 col-sm-4 col-md-4 wow fadeInLeft" data-wow-duration="1s">
                <img class="icon" src='<c:url value="images/icon_chatluong.png" />' alt="" /><br/>
                <span style="font-size: 32px; font-weight: 500;">CHẤT LƯỢNG</span><br/>
                <span>Chúng tôi cam kết sẽ mang đến cho các bạn chất lượng sản phẩm tốt nhất</span>
            </div>

            <div class="col-12 col-sm-4 col-md-4 wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s">
                <img class="icon" src='<c:url value="images/icon_tietkiemchiphi.png" />' alt="" /><br/>
                <span style="font-size: 32px; font-weight: 500;">TIẾT KIỆM CHI PHÍ</span><br/>
                <span>Cam kết giá rẻ nhất Việt Nam giúp các bạn tiết kiệm hơn 20% cho từng sản phẩm</span>
            </div>

            <div class="col-12 col-sm-4 col-md-4 wow fadeInUp" data-wow-duration="1s" data-wow-delay="2s">
                <img class="icon" src='<c:url value="images/icon_giaohang.jpg" />' alt="" /><br/>
                <span style="font-size: 32px; font-weight: 500;">GIAO HÀNG</span><br/>
                <span>Cam kết giao hàng tận nơi trong ngày. Để mang sản phẩm đến cho khách hàng nhanh nhất</span>
            </div>
        </div>
    </div>

    <!--Sản phẩm hot-->
    <div id="title-product" class="container">
        <span>SẢN PHẨM HOT</span>
        <div class="row" style="margin-top:42px;">
            <c:forEach var="list" items="${listSanPham}">
                <div class="col-md-3 col-sm-6">
                    <a href="/chitiet?idsanpham=${list.getIdSanPham()}">
                        <div class="sanpham wow bounceIn">
                            <img src='<c:url value="images/${list.getHinhSanPham()}" />' alt="" /><br>
                            <span>${list.getTenSanPham()}</span><br>
                            <span class="giatien">${list.getGiaTien()} VNĐ</span>
                        </div>
                    </a>
                </div>
            </c:forEach>
        </div>
    </div><!--End sản phẩm hot-->
    <!--End Body-->

    <!-- Footer -->
    <jsp:include page="/WEB-INF/views/footer.jsp"></jsp:include>
    <!-- End Footer -->
</body>
</html>