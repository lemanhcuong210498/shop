-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: shop
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chitiethoadon`
--

DROP TABLE IF EXISTS `chitiethoadon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chitiethoadon` (
  `idhoadon` int(11) NOT NULL,
  `idchitietsanpham` int(11) NOT NULL,
  `soluong` int(11) DEFAULT NULL,
  `giatien` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idhoadon`,`idchitietsanpham`),
  KEY `fk_chitiethoadon_chitietsanpham` (`idchitietsanpham`),
  CONSTRAINT `fk_chitiethoadon_chitietsanpham` FOREIGN KEY (`idchitietsanpham`) REFERENCES `chitietsanpham` (`idchitietsanpham`),
  CONSTRAINT `fk_chitiethoadon_hoadon` FOREIGN KEY (`idhoadon`) REFERENCES `hoadon` (`idhoadon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chitiethoadon`
--

LOCK TABLES `chitiethoadon` WRITE;
/*!40000 ALTER TABLE `chitiethoadon` DISABLE KEYS */;
INSERT INTO `chitiethoadon` VALUES (1,6,1,'245,000'),(2,4,1,'245,000');
/*!40000 ALTER TABLE `chitiethoadon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chitietkhuyenmai`
--

DROP TABLE IF EXISTS `chitietkhuyenmai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chitietkhuyenmai` (
  `idkhuyenmai` int(11) NOT NULL,
  `idsanpham` int(11) NOT NULL,
  PRIMARY KEY (`idkhuyenmai`,`idsanpham`),
  KEY `fk_chitietkhuyenmai_sanpham` (`idsanpham`),
  CONSTRAINT `fk_chitietkhuyenmai_khuyenmai` FOREIGN KEY (`idkhuyenmai`) REFERENCES `khuyenmai` (`idkhuyenmai`),
  CONSTRAINT `fk_chitietkhuyenmai_sanpham` FOREIGN KEY (`idsanpham`) REFERENCES `sanpham` (`idsanpham`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chitietkhuyenmai`
--

LOCK TABLES `chitietkhuyenmai` WRITE;
/*!40000 ALTER TABLE `chitietkhuyenmai` DISABLE KEYS */;
/*!40000 ALTER TABLE `chitietkhuyenmai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chitietsanpham`
--

DROP TABLE IF EXISTS `chitietsanpham`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chitietsanpham` (
  `idchitietsanpham` int(11) NOT NULL AUTO_INCREMENT,
  `idsanpham` int(11) DEFAULT NULL,
  `idsizesanpham` int(11) DEFAULT NULL,
  `idmausanpham` int(11) DEFAULT NULL,
  `soluong` int(11) DEFAULT NULL,
  `ngaynhap` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idchitietsanpham`),
  KEY `fk_chitietsanpham_mausanpham` (`idmausanpham`),
  KEY `fk_chitietsanpham_sanpham` (`idsanpham`),
  KEY `fk_chitietsanpham_sizesanpham` (`idsizesanpham`),
  CONSTRAINT `fk_chitietsanpham_mausanpham` FOREIGN KEY (`idmausanpham`) REFERENCES `mausanpham` (`idmausanpham`),
  CONSTRAINT `fk_chitietsanpham_sanpham` FOREIGN KEY (`idsanpham`) REFERENCES `sanpham` (`idsanpham`),
  CONSTRAINT `fk_chitietsanpham_sizesanpham` FOREIGN KEY (`idsizesanpham`) REFERENCES `sizesanpham` (`idsizesanpham`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chitietsanpham`
--

LOCK TABLES `chitietsanpham` WRITE;
/*!40000 ALTER TABLE `chitietsanpham` DISABLE KEYS */;
INSERT INTO `chitietsanpham` VALUES (1,2,1,1,30,'21/04/2020'),(2,2,2,2,60,'14/08/2020'),(3,2,3,3,80,'11/08/2020'),(4,3,1,1,80,'10/08/2020'),(5,3,2,2,90,'09/08/2020'),(6,3,3,4,110,'09/08/2020'),(7,4,1,1,500,'09/08/2020'),(8,4,2,5,300,'09/08/2020'),(9,4,3,2,20,'09/08/2020'),(10,5,1,1,10,'17/01/2020'),(11,5,2,2,50,'17/01/2020'),(12,5,3,1,80,'17/01/2020'),(13,6,1,1,80,'17/01/2020'),(14,6,2,2,80,'17/01/2020'),(15,6,3,2,80,'17/01/2020'),(16,7,1,1,300,'21/04/2020'),(17,7,2,6,300,'21/04/2020'),(18,7,3,1,300,'21/04/2020'),(19,8,1,1,500,'09/08/2020'),(20,8,2,2,500,'09/08/2020'),(21,8,3,2,500,'09/08/2020'),(22,9,1,1,20,'10/08/2020'),(23,9,2,7,20,'10/08/2020'),(24,9,3,1,20,'11/08/2020'),(25,10,1,1,60,'15/08/2020'),(26,10,2,2,60,'15/08/2020'),(27,10,3,2,60,'15/08/2020'),(28,11,1,1,90,'10/06/2020'),(29,11,2,2,90,'10/06/2020'),(30,11,3,1,90,'10/06/2020');
/*!40000 ALTER TABLE `chitietsanpham` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chucvu`
--

DROP TABLE IF EXISTS `chucvu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chucvu` (
  `idchucvu` int(11) NOT NULL AUTO_INCREMENT,
  `tenchucvu` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idchucvu`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chucvu`
--

LOCK TABLES `chucvu` WRITE;
/*!40000 ALTER TABLE `chucvu` DISABLE KEYS */;
INSERT INTO `chucvu` VALUES (1,'Nhân viên'),(2,'Admin'),(3,'Người dùng');
/*!40000 ALTER TABLE `chucvu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `danhmucsanpham`
--

DROP TABLE IF EXISTS `danhmucsanpham`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `danhmucsanpham` (
  `iddanhmucsanpham` int(11) NOT NULL AUTO_INCREMENT,
  `tendanhmuc` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`iddanhmucsanpham`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `danhmucsanpham`
--

LOCK TABLES `danhmucsanpham` WRITE;
/*!40000 ALTER TABLE `danhmucsanpham` DISABLE KEYS */;
INSERT INTO `danhmucsanpham` VALUES (1,'Áo sơ mi'),(2,'Áo thun'),(3,'Quần short'),(4,'Áo khoác'),(5,'Áo polo');
/*!40000 ALTER TABLE `danhmucsanpham` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hoadon`
--

DROP TABLE IF EXISTS `hoadon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hoadon` (
  `idhoadon` int(11) NOT NULL AUTO_INCREMENT,
  `tenkhachhang` varchar(50) DEFAULT NULL,
  `sodienthoai` varchar(12) DEFAULT NULL,
  `diachigiaohang` varchar(255) DEFAULT NULL,
  `tinhtrang` bit(1) DEFAULT NULL,
  `ngaylap` varchar(50) DEFAULT NULL,
  `hinhthucgiaohang` varchar(45) DEFAULT NULL,
  `ghichu` text,
  PRIMARY KEY (`idhoadon`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hoadon`
--

LOCK TABLES `hoadon` WRITE;
/*!40000 ALTER TABLE `hoadon` DISABLE KEYS */;
INSERT INTO `hoadon` VALUES (1,'a','0125736489','aaaa',_binary '\0',NULL,'Giao hàng tận nơi','aaaa'),(2,'a','0125736489','aaaa',_binary '\0',NULL,'Giao hàng tận nơi','aa');
/*!40000 ALTER TABLE `hoadon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `khuyenmai`
--

DROP TABLE IF EXISTS `khuyenmai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `khuyenmai` (
  `idkhuyenmai` int(11) NOT NULL AUTO_INCREMENT,
  `tenkhuyenmai` varchar(200) DEFAULT NULL,
  `thoigianbatdau` varchar(50) DEFAULT NULL,
  `thoigianketthuc` varchar(50) DEFAULT NULL,
  `mota` text,
  `hinhkhuyenmai` text,
  `giagiam` int(11) DEFAULT NULL,
  PRIMARY KEY (`idkhuyenmai`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `khuyenmai`
--

LOCK TABLES `khuyenmai` WRITE;
/*!40000 ALTER TABLE `khuyenmai` DISABLE KEYS */;
/*!40000 ALTER TABLE `khuyenmai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mausanpham`
--

DROP TABLE IF EXISTS `mausanpham`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mausanpham` (
  `idmausanpham` int(11) NOT NULL AUTO_INCREMENT,
  `tenmau` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idmausanpham`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mausanpham`
--

LOCK TABLES `mausanpham` WRITE;
/*!40000 ALTER TABLE `mausanpham` DISABLE KEYS */;
INSERT INTO `mausanpham` VALUES (1,'Đỏ'),(2,'Vàng'),(3,'Trắng'),(4,'Hồng'),(5,'Xanh dương'),(6,'Xanh lá'),(7,'Xanh ngọc');
/*!40000 ALTER TABLE `mausanpham` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nhanvien`
--

DROP TABLE IF EXISTS `nhanvien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nhanvien` (
  `idnhanvien` int(11) NOT NULL AUTO_INCREMENT,
  `hoten` varchar(50) DEFAULT NULL,
  `diachi` varchar(100) DEFAULT NULL,
  `gioitinh` bit(1) DEFAULT NULL,
  `cmnd` varchar(14) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tendangnhap` varchar(50) DEFAULT NULL,
  `matkhau` varchar(50) DEFAULT NULL,
  `idchucvu` int(11) DEFAULT NULL,
  PRIMARY KEY (`idnhanvien`),
  KEY `fk_idnhanvien_idchucvu` (`idchucvu`),
  CONSTRAINT `fk_idnhanvien_idchucvu` FOREIGN KEY (`idchucvu`) REFERENCES `chucvu` (`idchucvu`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nhanvien`
--

LOCK TABLES `nhanvien` WRITE;
/*!40000 ALTER TABLE `nhanvien` DISABLE KEYS */;
INSERT INTO `nhanvien` VALUES (1,'Lê Mạnh Cường','Nam Định',_binary '','036912345678','lemanhcuong0498@gmail.com','cuonglm','123',2),(3,NULL,NULL,_binary '\0',NULL,'123abc@gmail.com','123abc@gmail.com','123',NULL);
/*!40000 ALTER TABLE `nhanvien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sanpham`
--

DROP TABLE IF EXISTS `sanpham`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sanpham` (
  `idsanpham` int(11) NOT NULL AUTO_INCREMENT,
  `iddanhmucsanpham` int(11) DEFAULT NULL,
  `tensanpham` varchar(200) DEFAULT NULL,
  `giatien` varchar(50) DEFAULT NULL,
  `mota` text,
  `hinhsanpham` varchar(250) DEFAULT NULL,
  `gianhcho` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsanpham`),
  KEY `fk_sanpham_dmsanpham` (`iddanhmucsanpham`),
  CONSTRAINT `fk_sanpham_dmsanpham` FOREIGN KEY (`iddanhmucsanpham`) REFERENCES `danhmucsanpham` (`iddanhmucsanpham`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sanpham`
--

LOCK TABLES `sanpham` WRITE;
/*!40000 ALTER TABLE `sanpham` DISABLE KEYS */;
INSERT INTO `sanpham` VALUES (2,1,'Áo sơ mi kẻ sọc in ngực ASM016 màu xanh','275,000','- Thoáng mát, bền màu,  họa tiết in ngực năng động và trẻ trung, thiết kế kể sọc dọc tôn dáng thêm cao ráo, form suông lên đồ vừa vặn hơn.','aosomikesoc.png','nam'),(3,1,'Áo sơ mi nẹp dấu nút ASM1300 màu đỏ dô','245,000','- Thiết kế Slimfit ưu tiên những đường may vừa vặn, hạn chế ôm sát nhằm phù hợp với mọi vóc dáng. Form dáng phổ biến này giúp tăng thêm tính tiện ích và năng động cho trang phục.','ao-so-mi-theu-enjoy-holiday-mau-den-asm1299_2_small-14924-t.jpg','nam'),(4,2,'Áo thun in ngực AT025 màu xám','215,000','- Đa dạng gam màu, dễ dàng mix&match với mọi phong cách.','ao-thun-in-nguc-at025-mau-trang_2_small-15410.png','nam'),(5,2,'Áo thun trơn căn bản form slimfit AT018','215,000','- Đa dạng gam màu, dễ dàng mix&match với mọi phong cách.','ao-thun-tron-can-ban-form-slimfit-at018-mau-trang_2_small-15368-t.png','nam'),(6,3,'Quần short jean lưng thun QS202','345,000','- Form slim-fit ôm gọn gàng cơ thể, lưng thun sau co giãn năng động. Mặc trong nhà hay vận động ngoài trời đều thoải mái.','quan-short-jean-lung-thun-qs202-mau-xanh-bien_2_small-15205-t.png','nam'),(7,3,'Quần short xanh đen QS145','300,000','- Quần Short Xanh Đen QS145 chất jean dày dặn, bền, có độ co giãn nhẹ, thấm hút tốt. Kiểu dáng năng động, thoải mái, wax rách bụi bặm, ngầu.','quan-short-xanh-qs157_2_small-9976-t.jpg','nam'),(8,4,'Áo khoác da nâu AK258','645,000','- N/a','ao-khoac-da-nau-ak258_2_small-10459-t.jpg','nam'),(9,4,'Áo khoác da đen AK258','645,000',' - N/a','ao-khoac-da-den-ak258-10458-slide-products-5c2f17168eb24.jpg','nam'),(10,5,'Áo polo ngực thêu xương rồng PO002 màu trắng','275,000','- Bền bỉ, mềm mại, thấm hút tốt và giữ mùi thơm lâu.Polo - phép lai hoàn hảo giữa áo thun và sơ mi, điều này khiến Polo trở thành lựa chọn hàng đầu cho phái mạnh khi lựa chọn phong cách thoải mái nhưng vẫn sở hữu chỉnh chu cần thiết.','ao-polo-nguc-theu-xuong-rong-po002-mau-ca-phe-15418-slide-products-5f365b0446f6c.png','nam'),(11,5,'Áo thun polo lưng in chữ AT860 màu đỏ','275,000','- Bền bỉ, mềm mại, thấm hút tốt và giữ mùi thơm lâu.Polo - phép lai hoàn hảo giữa áo thun và sơ mi, điều này khiến Polo trở thành lựa chọn hàng đầu cho phái mạnh khi lựa chọn phong cách thoải mái nhưng vẫn sở hữu chỉnh chu cần thiết.','ao-thun-polo-lung-in-chu-at860-15019-slide-products-5e155b5fab41c.jpg','nam');
/*!40000 ALTER TABLE `sanpham` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sizesanpham`
--

DROP TABLE IF EXISTS `sizesanpham`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sizesanpham` (
  `idsizesanpham` int(11) NOT NULL AUTO_INCREMENT,
  `size` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idsizesanpham`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sizesanpham`
--

LOCK TABLES `sizesanpham` WRITE;
/*!40000 ALTER TABLE `sizesanpham` DISABLE KEYS */;
INSERT INTO `sizesanpham` VALUES (1,'M'),(2,'L'),(3,'XL');
/*!40000 ALTER TABLE `sizesanpham` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'shop'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-09 21:04:03
